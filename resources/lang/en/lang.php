<?php

return [
    'common' => [
        'success'   => 'Success',
        'error'     => 'Error',
        'not-found' => ':title not found.',
    ],
    'crud' => [
        'destroy' => ':title deleted successfully.',
    ],
    'user' => [
        'full_name' => 'Full Name',
        'gsm'       => 'GSM Number',
        'email'     => 'E-Mail',
        'email_gsm' => 'E-Mail or GSM number',
        'password'  => 'Password',
    ],
    'auth' => [
        'error'           => 'Incorrect information. Please try again.',
        'unauthenticated' => 'User hasn\'t signed in.',
    ],
    'address'            => [
        'lat_lang_format_error' => 'Your location unable to be received properly.',
        'not_delete'            => 'You can\'t delete your active address.',
        'type'                  => 'Address Type',
        'title'                 => 'Title',
        'address'               => 'Address',
        'address_recipe'        => 'Directions',
        'building'              => 'Building',
        'floor'                 => 'Floor',
        'apartment'             => 'Apartment',
        'lat_lng'               => 'Location',
    ],
];
