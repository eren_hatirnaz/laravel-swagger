<?php

return [
    'common' => [
        'success'   => 'Başarılı',
        'error'     => 'Hata',
        'not-found' => ':title bulunamadı.',
    ],
    'crud' => [
        'destroy' => ':title başarıyla silindi.',
    ],
    'user' => [
        'full_name' => 'Ad Soyad',
        'gsm'       => 'Telefon Numarası',
        'email'     => 'E-Posta Adresi',
        'email_gsm' => 'E-Posta Adresi ya da Telefon Numarası',
        'password'  => 'Parola',
    ],
    'auth' => [
        'error'           => 'Bilgileriniz hatalıdır. Lütfen tekrar deneyiniz.',
        'unauthenticated' => 'Kullanıcı giriş yapmamış.',
    ],
    'address' => [
        'lat_lang_format_error' => 'Konumunuz düzgün alınamadı.',
        'not_delete'            => 'Aktif adresinizi silemezsiniz.',
        'type'                  => 'Adres Tipi',
        'title'                 => 'Başlık',
        'address'               => 'Adres',
        'address_recipe'        => 'Adres Tarifi',
        'building'              => 'Bina',
        'floor'                 => 'Kat',
        'apartment'             => 'Daire',
        'lat_lng'               => 'Konum',
    ],
];
