FROM php:8
RUN apt-get update -y && apt-get install -y openssl zip unzip git libonig-dev libxml2-dev libcurl4-openssl-dev libssl-dev zlib1g-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev libedit-dev
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo pdo_mysql opcache xml calendar curl exif fileinfo ftp gd gettext iconv mbstring mysqli phar readline shmop simplexml sockets sysvmsg sysvsem
WORKDIR /app
COPY . /app
RUN composer install

CMD php artisan serve --host=0.0.0.0 --port=80
EXPOSE 80
