<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $language = $request->header('Accept-Language');

        $this->setLanguage($language, $request);

        return $next($request);
    }

    public function setLanguage($language, $request)
    {
        $language = substr($language, 0, 2);

        if (in_array($language, ["en", "tr"])){
            app()->setLocale($language);
            Carbon::setLocale($language);
        }
    }
}
