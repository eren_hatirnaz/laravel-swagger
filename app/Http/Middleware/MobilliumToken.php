<?php

namespace App\Http\Middleware;

use App\Models\Token;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Contracts\Auth\Guard;

class MobilliumToken
{
    /**
     * @var Guard
     */
    protected $auth;

    /**
     * AuthenticateApi constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();

        if ($token) {
            $userId = Cache::remember('api_token_' . $token, 60, function () use ($token) {
                $token = Token::whereToken($token)->first();
                return $token->user_id ?? null;
            });

            if ($userId) {
                $this->auth->onceUsingId($userId);
            }
        }

        return $next($request);
    }
}
