<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *      schema="AddressResource",
 *      @OA\Property(property="id", type="integer", description="Address ID", example=1),
 *      @OA\Property(property="type", type="string", enum={"home", "work", "other"}, description="Address type", example="home"),
 *      @OA\Property(property="title", type="string", minLength=2, description="Title", example="home"),
 *      @OA\Property(property="address", type="string", minLength=10, description="Address", example="Bahçeşehir 2. Kısım 12. Caddesi no: 12I İstanbul Başakşehir"),
 *      @OA\Property(property="address_recipe", type="string", minLength=2, description="Address recipe", example="X dükkan karşısı"),
 *      @OA\Property(property="building", type="string", description="Building number", example=12),
 *      @OA\Property(property="floor", type="string", description="Floor", example=8),
 *      @OA\Property(property="apartment", type="string", description="Door number", example=27),
 *      @OA\Property(property="lat_lng", type="string", nullable=true, description="Coordinat. Format: LAT, LONG", example="41.084422821772804, 28.672636912603974"),
 *      @OA\Property(property="is_active", type="boolean", description="Is the address active or not", example=true),
 *      @OA\Property(property="city", type="string", nullable=true, description="City", example="İstanbul"),
 *      @OA\Property(property="county", type="string", nullable=true, description="County", example="Başakşehir"),
 * )
 */

class Address extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'type'           => $this->type,
            'title'          => $this->title,
            'address'        => $this->address,
            'address_recipe' => $this->address_recipe,
            'building'       => $this->building,
            'floor'          => $this->floor,
            'apartment'      => $this->apartment,
            'lat_lng'        => $this->lat_lng,
            'is_active'      => $this->is_active,
            'city'           => $this->city,
            'county'         => $this->county,
        ];
    }
}
