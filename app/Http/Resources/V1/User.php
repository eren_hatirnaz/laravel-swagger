<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Schema(
 *      schema="UserResource",
 *      @OA\Property(property="id", type="integer", description="User ID", example=1),
 *      @OA\Property(property="gsm", type="string", description="GSM Number", example="5123456789"),
 *      @OA\Property(property="email", type="string", format="email", description="E-mail address", example="john.doe@example.com"),
 *      @OA\Property(property="full_name", type="string", minLength=2, description="Name and surname", example="John Doe"),
 *      @OA\Property(property="is_active", type="boolean", default=true, description="Is the user active or not", example=1)
 * )
 */
class User extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'gsm'       => $this->gsm,
            'email'     => $this->email,
            'full_name' => $this->full_name,
            'is_active' => $this->is_active,
        ];
    }
}
