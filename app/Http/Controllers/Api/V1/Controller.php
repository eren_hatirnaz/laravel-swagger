<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      title="Laravel Swagger",
 *      version="1.0",
 * )
 *
 * @OA\Parameter(
 *      parameter="AcceptLanguage",
 *      in="header",
 *      name="Accept-Language",
 *      description="Application language",
 *      @OA\Schema(type="string", enum={"tr", "en"}, default="tr")
 * )
 *
 * @OA\Schema(
 *      schema="Error",
 *      @OA\Property(property="code", type="string", description="Error code", example="common.not-found"),
 *      @OA\Property(property="title", type="string", description="Title", example="Error"),
 *      @OA\Property(property="message", type="string", description="Message", example="Address not found."),
 *
 * )
 * @OA\Schema(
 *      schema="Message",
 *      @OA\Property(property="code", type="string", description="Message", example="crud.destroy"),
 *      @OA\Property(property="title", type="string", description="Başlık", example="Success"),
 *      @OA\Property(property="message", type="string", description="Mesaj", example="Address deleted successfully."),
 * )
 * @OA\Schema(
 *      schema="Success",
 *      @OA\Property(property="code", type="string", description="Kod", example="common.success"),
 *      @OA\Property(property="message", type="string", description="Mesaj", example="Success"),
 *      @OA\Property(property="data", description="Data. This could be an `array` or an `object`."),
 * )
 *
 * @OA\Examples(
 *      example="ValidationErrorExample",
 *      summary="Validation error",
 *      value={"code": "validation", "title": "Error", "message": "Validation error message (This field changes by the validation rule)"}
 * )
 *
 * @OA\Response(
 *      response="ValidationErrorResponse",
 *      description="Validation error",
 *      @OA\JsonContent(
 *          allOf={@OA\Schema(ref="#/components/schemas/Error")},
 *          examples={
 *              @OA\Examples(example="ValidationErrorExample", ref="#/components/examples/ValidationErrorExample")
 *          }
 *      )
 * )
 *
 * @OA\Response(
 *      response="UnauthorizedResponse",
 *      description="Unauthorized",
 *      @OA\JsonContent(
 *          ref="#/components/schemas/Error",
 *          example={"code": "auth.unauthenticated", "title": "Error", "message": "User hasn't signed in."}
 *      )
 * )
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
