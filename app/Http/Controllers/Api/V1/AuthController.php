<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\Api\V1\LoginRequest;
use App\Http\Requests\Api\V1\RegisterRequest;
use App\Http\Resources\V1\User as UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Tag(
 *      name="Auth",
 *      description="User login, register and etc.",
 * )
 *
 *  @OA\Response(
 *      response="LoginSuccessResponse",
 *      description="Logged in successfully.",
 *      @OA\JsonContent(
 *          allOf={@OA\Schema(ref="#/components/schemas/Success")},
 *          @OA\Property(
 *              property="data",
 *              type="object",
 *              description="Token and user information",
 *              @OA\Property(property="token", type="string", format="uuid", description="Token", example="e706346f-2741-4483-9645-7999e668e2df"),
 *              @OA\Property(property="user", type="object", description="User information", ref="#/components/schemas/UserResource"),
 *          )
 *      )
 * )
 */
class AuthController extends Controller
{
    /**
     * User login
     *
     * @OA\Post(
     *      path="/api/v1/auth/login",
     *      tags={"Auth"},
     *      operationId="auth_login",
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\RequestBody(ref="#/components/requestBodies/LoginRequest"),
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Error",
     *              example={"code": "auth.error", "title": "Hata", "message": "Incorrect information. Please try again."}
     *          )
     *      ),
     *      @OA\Response(response=200, ref="#/components/responses/LoginSuccessResponse")
     * )
     *
     */
    public function login(LoginRequest $request)
    {
        $emailGsm = $request->input('email_gsm');
        $password = $request->input('password');

        $user = User::query()->gsmOrEmail($emailGsm)->first();
        if (!$user) {
            return response()->error('auth.error');
        }

        if (!(Auth::attempt(['email' => $emailGsm, 'password' => $password]) || Auth::attempt(['gsm' => $emailGsm, 'password' => $password]))) {
            return response()->error('auth.error');
        }

        $response = [
            "token" => $user->login()["token"],
            "user" => new UserResource($user),
        ];

        return response()->success($response);
    }

    /**
     * User register
     *
     * @OA\Post(
     *      path="/api/v1/auth/register",
     *      tags={"Auth"},
     *      operationId="auth_register",
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\RequestBody(ref="#/components/requestBodies/RegisterRequest"),
     *      @OA\Response(response=400, ref="#/components/responses/ValidationErrorResponse"),
     *      @OA\Response(response=200, ref="#/components/responses/LoginSuccessResponse"),
     * )
     *
     */
    public function register(RegisterRequest $request)
    {
        $user = new User();
        $user->fill($request->only(['full_name', 'email', 'password']));
        $user->password = $request->input('password');
        $user->is_active = 1;
        $user->save();

        $response = [
            "token" => $user->login()["token"],
            "user" => new UserResource($user),
        ];

        return response()->success($response);
    }
}
