<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\V1\User as UserResource;
use App\Http\Requests\Api\V1\UserRequest as UserRequest;
use Illuminate\Http\Request;

/**
 * @OA\Tag(
 *      name="User",
 *      description="Everything about the logged-in user"
 * )
 */
class UserController extends Controller
{
    /**
     * Display the logged-in User Resource.
     *
     * @OA\Get(
     *      path="/api/v1/user/me",
     *      tags={"User"},
     *      operationId="user_show",
     *      security={{"token": {}}},
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\Response(response=401, ref="#/components/responses/UnauthorizedResponse"),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(
     *              allOf={@OA\Schema(ref="#/components/schemas/Success")},
     *              @OA\Property(property="data", type="object", ref="#/components/schemas/UserResource")
     *          )
     *      )
     * )
     *
     *
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();

        return response()->success(new UserResource($user));
    }


    /**
     * Update the logged-in User Resource in storage.
     *
     * @OA\Put(
     *      path="/api/v1/user/me",
     *      tags={"User"},
     *      operationId="user_update",
     *      security={{"token": {}}},
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\RequestBody(ref="#/components/requestBodies/UserRequest"),
     *      @OA\Response(response=401, ref="#/components/responses/UnauthorizedResponse"),
     *      @OA\Response(response=400, ref="#/components/responses/ValidationErrorResponse"),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(
     *              allOf={@OA\Schema(ref="#/components/schemas/Success")},
     *              @OA\Property(property="data", type="object", ref="#/components/schemas/UserResource"),
     *          )
     *      )
     * )
     */
    public function update(UserRequest $request, $id)
    {
        $user = $request->user();

        $user->fill($request->only(['full_name', 'email']));
        $user->save();

        return response()->success(new UserResource($user));
    }
}
