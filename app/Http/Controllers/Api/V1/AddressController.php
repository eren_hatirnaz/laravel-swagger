<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\V1\AddressRequest;
use Illuminate\Http\Request;
use App\Http\Resources\V1\Address as AddressResource;
use App\Models\Address;

/**
 * @OA\Tag(
 *      name="Address",
 *      description="Everything about addresses belongs to the logged-in user",
 * )
 *
 * @OA\Parameter(
 *      parameter="AddressIdParameter",
 *      in="path",
 *      name="addressId",
 *      @OA\Schema(type="integer"),
 *      required=true,
 *      description="Address ID",
 *      example="1"
 * )
 *
 * @OA\Examples(
 *      example="LatLongFormatErrorExample",
 *      summary="Lat long format error",
 *      value={"code": "address.lat_lang_format_error", "title": "Error", "message": "Your location unable to be received properly."}
 * )
 * @OA\Examples(
 *      example="AddressNotFoundExample",
 *      summary="Adres not found",
 *      value={"code": "common.not-found", "title": "Error", "message": "Address not found."}
 * )
 * @OA\Examples(
 *      example="AddressIsActiveErrorExample",
 *      summary="Address is active",
 *      value={"code": "address.not_delete", "title": "Error", "message": "You can't delete your active address."}
 * )
 */

class AddressController extends Controller
{
    /**
     * Display a listing of the Address Resource.
     *
     * @OA\Get(
     *      path="/api/v1/address",
     *      tags={"Address"},
     *      operationId="address_index",
     *      security={{"token":{}}},
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\Response(response=401, ref="#/components/responses/UnauthorizedResponse"),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(
     *              type="object",
     *              allOf={@OA\Schema(ref="#/components/schemas/Success")},
     *              @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/AddressResource"))
     *          )
     *      )
     * )
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $addresses = $user->addresses()->withPassive()->get();

        return response()->success(AddressResource::collection($addresses));
    }


    /**
     * Store a newly created Address Resource in storage.
     *
     * If the user already has an active address, the newly created address
     * becomes passive.
     *
     *
     * @OA\Post(
     *      path="/api/v1/address",
     *      tags={"Address"},
     *      operationId="address_store",
     *      security={{"token": {}}},
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\RequestBody(ref="#/components/requestBodies/AddressRequest"),
     *      @OA\Response(response=401, ref="#/components/responses/UnauthorizedResponse"),
     *      @OA\Response(
     *          response=400,
     *          description="Bad request",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Error",
     *              examples={
     *                  @OA\Examples(example="LatLongFormatErrorExample", ref="#/components/examples/LatLongFormatErrorExample"),
     *                  @OA\Examples(example="ValidationErrorExample", ref="#/components/examples/ValidationErrorExample")
     *              }
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Address created successfully",
     *          @OA\JsonContent(
     *              type="object",
     *              allOf={@OA\Schema(ref="#/components/schemas/Success")},
     *              @OA\Property(property="data", type="object", ref="#/components/schemas/AddressResource")
     *          )
     *      )
     * )
     *
     */
    public function store(AddressRequest $request)
    {
        $user = $request->user();

        $explode = explode(", ", $request->input("lat_lng"));
        $lat = $explode[0] ?? null;
        $lng = $explode[1] ?? null;

        if(!is_numeric($lat) || !is_numeric($lng)){
            return response()->error("address.lat_lang_format_error");
        }

        $request["lat"] = $lat;
        $request["lng"] = $lng;

        $activeAddress = $user->addresses()->where('is_active', 1)->first();
        if (!$activeAddress) {
            $request["is_active"] = 1;
        } else {
            $request["is_active"] = 0;
        }
        $address = $user->addresses()->create($request->input());


        return response()->success(new AddressResource($address));
    }

    /**
     * Display the specified Address Resource.
     *
     * @OA\Get(
     *      path="/api/v1/address/{addressId}",
     *      tags={"Address"},
     *      operationId="address_show",
     *      security={{"token": {}}},
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\Parameter(ref="#/components/parameters/AddressIdParameter"),
     *      @OA\Response(response=401, ref="#/components/responses/UnauthorizedResponse"),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Error",
     *              examples={
     *                  @OA\Examples(example="AddressNotFoundExample", ref="#/components/examples/AddressNotFoundExample")
     *              }
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(
     *              allOf={@OA\Schema(ref="#/components/schemas/Success")},
     *              @OA\Property(property="data", type="object", ref="#/components/schemas/AddressResource")
     *          )
     *      )
     * )
     *
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();

        $model = $user->addresses()->withPassive()->find($id);
        if(!$model){
            return response()->error('common.not-found', ['title' => trans('lang.address.address')]);
        }

        return response()->success(new AddressResource($model));
    }


    /**
     * Update the specified Address Resource in storage.
     *
     * @OA\Put(
     *      path="/api/v1/address/{addressId}",
     *      tags={"Address"},
     *      operationId="address_update",
     *      security={{"token": {}}},
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\Parameter(ref="#/components/parameters/AddressIdParameter"),
     *      @OA\RequestBody(ref="#/components/requestBodies/AddressRequest"),
     *      @OA\Response(response=401, ref="#/components/responses/UnauthorizedResponse"),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Error",
     *              examples={
     *                  @OA\Examples(example="LatLongFormatErrorExample", ref="#/components/examples/LatLongFormatErrorExample"),
     *                  @OA\Examples(example="AddressNotFoundExample", ref="#/components/examples/AddressNotFoundExample"),
     *                  @OA\Examples(example="ValidationErrorExample", ref="#/components/examples/ValidationErrorExample")
     *              }
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Address updated successfully",
     *          @OA\JsonContent(
     *              type="object",
     *              allOf={@OA\Schema(ref="#/components/schemas/Success")},
     *              @OA\Property(property="data", type="object", ref="#/components/schemas/AddressResource")
     *          )
     *      )
     * )
     */
    public function update(AddressRequest $request, $id)
    {
        $user = $request->user();

        $model = $user->addresses()->withPassive()->find($id);
        if(!$model){
            return response()->error('common.not-found', ['title' => trans('lang.address.address')]);
        }

        $inputs = $request->input();
        $latLng = $request->input('lat_lng');
        if($model->lat_lng != $latLng) {
            $explode = explode(", ", $latLng);
            $lat = $explode[0] ?? null;
            $lng = $explode[1] ?? null;
            if(!is_numeric($lat) || !is_numeric($lng)){
                return response()->error("address.lat_lang_format_error");
            }

            $request["lat"] = $lat;
            $request["lng"] = $lng;
        }

        Address::query()->withPassive()->find($id)->update($inputs);

        $model = Address::query()->withPassive()->find($id);

        return response()->success($model);
    }

    /**
     * Remove the specified Address Resource from storage.
     *
     * @OA\Delete(
     *      path="/api/v1/address/{addressId}",
     *      tags={"Address"},
     *      operationId="address_destroy",
     *      security={{"token": {}}},
     *      @OA\Parameter(ref="#/components/parameters/AcceptLanguage"),
     *      @OA\Parameter(ref="#/components/parameters/AddressIdParameter"),
     *      @OA\Response(response=401, ref="#/components/responses/UnauthorizedResponse"),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Error",
     *              examples={
     *                  @OA\Examples(example="AddressNotFoundExample", ref="#/components/examples/AddressNotFoundExample"),
     *                  @OA\Examples(example="AddressIsActiveErrorExample", ref="#/components/examples/AddressIsActiveErrorExample"),
     *              }
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Address deleted successfully.",
     *          @OA\JsonContent(
     *              ref="#/components/schemas/Message",
     *              example={"code": "crud.destroy", "title": "Success", "message": "Address deleted successfully."}
     *          )
     *      )
     * )
     */
    public function destroy(Request $request, $id)
    {
        $user = $request->user();

        $model = $user->addresses()->withPassive()->find($id);
        if(!$model){
            return response()->error('common.not-found', ['title' => trans('lang.address.address')]);
        }

        if($model->is_active){
            return response()->error('address.not_delete');
        }

        $model->delete();

        return response()->message('crud.destroy', ['title' => trans('lang.address.address') ]);
    }
}
