<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;

/**
 * @OA\RequestBody(
 *      request="UserRequest",
 *      required=true,
 *      @OA\JsonContent(
 *          required={"full_name", "email"},
 *          @OA\Property(property="full_name",ref="#/components/schemas/UserResource/properties/full_name"),
 *          @OA\Property(property="email", ref="#/components/schemas/UserResource/properties/email"),
 *      )
 * )
 */

class UserRequest extends Request
{
    public function rules()
    {
        $id = $this->route()->parameter('user');
        if ($id == 'me') {
            $id = $this->user()->id;
        }

        return [
            'full_name' => 'required|min:2',
            'email'     => 'required|email|unique:users,email,' . $id,
        ];
    }

    public function attributes()
    {
        return [
            'full_name' => trans('models.user.full_name'),
            'email'     => trans('models.user.email'),
        ];
    }
}
