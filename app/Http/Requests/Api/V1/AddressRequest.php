<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;

/**
 * @OA\RequestBody(
 *      request="AddressRequest",
 *      @OA\JsonContent(
 *          required={"type", "title", "address", "address_recipe", "building", "floor", "apartment", "lat_lng"},
 *          @OA\Property(property="type", ref="#/components/schemas/AddressResource/properties/type"),
 *          @OA\Property(property="title", ref="#/components/schemas/AddressResource/properties/title"),
 *          @OA\Property(property="address", ref="#/components/schemas/AddressResource/properties/address"),
 *          @OA\Property(property="address_recipe", ref="#/components/schemas/AddressResource/properties/address_recipe"),
 *          @OA\Property(property="building", ref="#/components/schemas/AddressResource/properties/building"),
 *          @OA\Property(property="floor", ref="#/components/schemas/AddressResource/properties/floor"),
 *          @OA\Property(property="apartment", ref="#/components/schemas/AddressResource/properties/apartment"),
 *          @OA\Property(property="lat_lng", ref="#/components/schemas/AddressResource/properties/lat_lng"),
 *          @OA\Property(property="city", ref="#/components/schemas/AddressResource/properties/city"),
 *          @OA\Property(property="county", ref="#/components/schemas/AddressResource/properties/county"),
 *      )
 * )
 */

class AddressRequest extends Request
{
/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'              => 'required',
            'title'             => 'required|min:2',
            'address'           => 'required|min:10',
            'address_recipe'    => 'required|min:2',
            'building'          => 'required',
            'floor'             => 'required',
            'apartment'         => 'required',
            'lat_lng'           => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'type'              => trans('models.address.type'),
            'title'             => trans('models.address.title'),
            'address'           => trans('models.address.address'),
            'address_recipe'    => trans('models.address.address_recipe'),
            'building'          => trans('models.address.building'),
            'floor'             => trans('models.address.floor'),
            'apartment'         => trans('models.address.apartment'),
            'lat_lng'           => trans('models.address.lat_lng'),
        ];
    }
}
