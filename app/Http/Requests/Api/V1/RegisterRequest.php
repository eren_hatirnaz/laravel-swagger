<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;

/**
 * @OA\RequestBody(
 *      request="RegisterRequest",
 *      required=true,
 *      @OA\JsonContent(
 *          required={"full_name", "email", "password"},
 *          @OA\Property(property="full_name", ref="#/components/schemas/UserResource/properties/full_name"),
 *          @OA\Property(property="email", ref="#/components/schemas/UserResource/properties/email"),
 *          @OA\Property(property="password", type="string", minLength=6, description="Parola"),
 *          examples={
 *              @OA\Examples(example="RegisterExample", summary="Register example", value={"full_name": "John Doe", "email": "john.doe@example.com", "password": "123456"}),
 *          }
 *      )
 * )
 */
class RegisterRequest extends Request
{
    public function rules()
    {
        return [
            'full_name' => 'required|min:2',
            'email'     => 'required|email|unique:users,email',
            'password'  => 'required|min:6',
        ];
    }
    public function attributes()
    {
        return [
            'full_name' => trans('models.user.full_name'),
            'email'     => trans('models.user.email'),
            'password'  => trans('models.user.password'),
        ];
    }
}
