<?php

namespace App\Http\Requests\Api\V1;

use App\Http\Requests\Request;

/**
 * @OA\RequestBody(
 *      request="LoginRequest",
 *      required=true,
 *      @OA\JsonContent(
 *          required={"email_gsm", "password"},
 *          @OA\Property(property="email_gsm", type="string", description="E-mail address or GSM Number"),
 *          @OA\Property(property="password", type="string", minLength=6, maxLength=20, description="Password"),
 *
 *          examples={
 *              @OA\Examples(example="LoginWithEmailExample", summary="Login with e-mail", value={"email_gsm": "john.doe@example.com", "password": "123456"}),
 *              @OA\Examples(example="LoginWithGSMNumberExample", summary="Login with gsm number", value={"email_gsm": "05123456789", "password": "654321"})
 *          }
 *      )
 * )
 */
class LoginRequest extends Request
{
    public function rules()
    {
        return [
            'email_gsm' => 'required',
            'password'  => 'required|min:6|max:20',
        ];
    }

    public function attributes()
    {
        return [
            'email_gsm' => trans('models.user.email_gsm'),
            'password'  => trans('models.user.password'),
        ];
    }
}
