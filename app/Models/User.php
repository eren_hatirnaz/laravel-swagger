<?php

namespace App\Models;

use App\Traits\Activatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class User extends Authenticatable
{
    use HasFactory, Activatable, SoftDeletes, Notifiable;

    protected $fillable = [
        'full_name',
        'gsm',
        'email',
        'password',
        'is_active',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    //SCOPE
    public function scopeGsmOrEmail($query, $gsmEmail)
    {
       return $query->where(function($query) use ($gsmEmail) {
            $query->where('gsm', $gsmEmail)
                ->orWhere('email', $gsmEmail);
        });
    }


    //ATTRIBUTE
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }


    //RELATIONSHIPS
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    public function tokens()
    {
        return $this->hasMany(Token::class);
    }

    //FUNCTION
    public function login()
    {
        $token = new Token();
        $token->token = Uuid::uuid4()->toString();
        $token->user()->associate($this);
        $token->save();

        return $token->fresh();
    }

}
