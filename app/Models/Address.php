<?php

namespace App\Models;

use App\Traits\Activatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use Activatable, SoftDeletes;

    protected $fillable = [
        'type',
        'title',
        'address',
        'address_recipe',
        'building',
        'floor',
        'apartment',
        'lat_lng',
        'is_active',
        'city',
        'county',
    ];

    protected $casts = [
        'is_active' => 'boolean',
    ];

    const TYPE_HOME = "home";
    const TYPE_WORK = "work";
    const TYPE_OTHER = "other";

    const TYPES = [
        self::TYPE_HOME,
        self::TYPE_WORK,
        self::TYPE_OTHER,
    ];

    //SCOPE


    //ATTRIBUTE


    //RELATIONSHIPS
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //FUNCTION


}
