<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ["home", "work", "other"]);
            $table->string('title');
            $table->text('address');
            $table->text('address_recipe');
            $table->string('building');
            $table->string('floor');
            $table->string('apartment');
            $table->string('lat_lng')->nullable();
            $table->boolean('is_active')->default(0);
            $table->integer('order')->default(0);
            $table->string('city')->nullable();
            $table->string('county')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
