<?php

use Illuminate\Support\Facades\Route;

Route::post('/auth/login', 'AuthController@login')->name('auth.login');
Route::post('/auth/register', 'AuthController@register')->name('auth.register');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController')->only(['show', 'update']);

    Route::resource('address', 'AddressController');
});
